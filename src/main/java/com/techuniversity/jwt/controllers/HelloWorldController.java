package com.techuniversity.jwt.controllers;

import com.techuniversity.jwt.components.JWTBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("techu/jwt")
public class HelloWorldController {

    @Autowired
    JWTBuilder jwtBuilder;

    @GetMapping("/tokenGet")
    public String tokenGet(@RequestParam(value = "nombre", defaultValue = "techu") String name) {
        return jwtBuilder.generarToken(name, "admin");
    }

    @GetMapping(path = "/hola", headers = {"Authorization"})
    public String hola(@RequestHeader ("Authorization") String token) {

        String userId;
        try {
            userId = jwtBuilder.validarToken(token);

        } catch (Exception ex) {
            return ex.getMessage();
        }
        String s = String.format("Saludos para %s desde la Demo de validar Token", userId);
        return s;
    }
}
