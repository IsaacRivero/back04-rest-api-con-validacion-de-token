package com.techuniversity.jwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JwtSeguridadApplication {

	public static void main(String[] args) {
		SpringApplication.run(JwtSeguridadApplication.class, args);
	}

}
